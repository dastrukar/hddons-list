import { initEntries } from "./entries.js";
import { showDialog } from "./dialog.js";

// the settings

export var Settings = {
	"hddons-list-theme": "nord",
	"max-entries": 20,
	"scale": "auto"
};

var Scales = {
	"auto": "Auto",
	"50%": "50%",
	"75%": "75%",
	"100%": "100%",
	"125%": "125%",
	"150%": "150%",
	"175%": "175%",
	"200%": "200%",
	"225%": "225%",
	"250%": "250%"
};

var Themes = {
	"nord": "Nord",
	"dracula": "Dracula",
	"monokai": "Monokai",
	"gruvbox-dark": "Gruvbox Dark",
	"gruvbox-light": "Gruvbox Light",
	"solarized-dark": "Solarized Dark",
	"solarized-light": "Solarized Light"
};

var UpdateOnDialogClose = false;

export function updateSettings()
{
	const theme = localStorage.getItem("hddons-list-theme");
	const maxEntries = localStorage.getItem("max-entries");
	const scale = localStorage.getItem("scale");

	if (!theme)
		localStorage.setItem("hddons-list-theme", Settings["hddons-list-theme"]);

	else
		Settings["hddons-list-theme"] = theme;
		document.documentElement.classList = theme;

	if (!maxEntries)
		localStorage.setItem("max-entries", Settings["max-entries"]);

	else if (Settings["max-entries"] != maxEntries)
	{
		Settings["max-entries"] = maxEntries;
		UpdateOnDialogClose = true;
	}

	if (!scale)
		localStorage.setItem("scale", Settings["scale"]);

	else
	{
		Settings["scale"] = scale;
		if (scale == "auto")
			document.documentElement.style.fontSize = "";

		else
			document.documentElement.style.fontSize = scale;
	}
}

export function initSettingsButton()
{
	const settings = document.getElementById("settings");
	settings.onclick = () =>
	{
		UpdateOnDialogClose = false;
		const dialog = document.getElementById("popup");
		dialog.innerHTML = "<h3>Configuration</h3><hr>";

		const theme = createSettingsSelect("Theme:", "hddons-list-theme", Themes);
		const scale = createSettingsSelect("Scale:", "scale", Scales);
		const pages = createSettingsNumber("Max entries:", "max-entries", 1, 100);

		dialog.appendChild(theme);
		dialog.appendChild(scale);
		dialog.appendChild(pages);

		const callback = () =>
		{
			if (!UpdateOnDialogClose)
				return;

			UpdateOnDialogClose = false;
			initEntries();
		}
		showDialog(callback);
	};
}

function createSettingsSelect(text, variable, options)
{
	const label = document.createElement("label");
	label.innerHTML = text;
	label.classList.add("settings");

	const select = document.createElement("select");

	select.addEventListener(
		"change",
		(event) =>
		{
			console.log(event.target.value);
			localStorage.setItem(variable, event.target.value);
			updateSettings();
		}
	);

	for (const key in options)
	{
		const option = document.createElement("option");
		option.value = key;
		option.innerHTML = options[key];
		select.appendChild(option);

		if (key == Settings[variable])
			select.value = option.value;
	}

	select.value = Settings[variable];

	label.appendChild(select);
	return label;
}

function createSettingsNumber(text, variable, min, max)
{
	const label = document.createElement("label");
	label.innerHTML = text;
	label.classList.add("settings");

	const input = document.createElement("input");
	input.type = "number";

	input.addEventListener(
		"change",
		(event) =>
		{
			console.log(event.target.valueAsNumber);
			if (event.target.valueAsNumber > max)
				event.target.value = max;

			else if (event.target.valueAsNumber < min || Number.isNaN(event.target.valueAsNumber))
				event.target.value = min;

			localStorage.setItem(variable, event.target.valueAsNumber);
			updateSettings();
		}
	);

	input.value = Settings[variable];

	label.appendChild(input);
	return label;
}

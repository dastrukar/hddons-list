import { SearchArgs, clearSearch } from "./search.js";

//
//     ####       ####
//     ####       ####
// #######################
// #######################
//     ####       ####
//     ####       ####
//     ####       ####
//     ####       ####
// #######################
// #######################
//     ####       ####
//     ####       ####
//

export function initHash()
{
	window.onhashchange = () =>
	{
		processHash();
		initEntries();
	};
}

export function clearHash()
{
	SearchArgs.ids = [];
	history.replaceState("", document.title, window.location.pathname);
}

export function processHash()
{
	const hash = location.hash.substr(1);

	const temp = hash.split(":");
	let id = "";
	if (temp.length > 1)
		id = temp[1];

	else
		id = temp[0];

	// TODO: add support for multiple ids
	// tmp fix for hashes not working when you search
	clearSearch();
	SearchArgs.ids.push(id);
	console.log(SearchArgs.ids);
}

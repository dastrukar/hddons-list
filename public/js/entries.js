import { API_LINK } from "./global_vars.js";
import { SearchArgs } from "./search.js";
import { Settings } from "./settings.js";
import { PageIndex, MaxPages, updatePageSelect } from "./page.js";
import { showDialog } from "./dialog.js";

// Handles entries

// The id of the addons section
const ENTRIES_ID = "entries";
const PINNED_ID = "pinned";

// This is very important
export var IsBuilding = false;

// Removes all addon entries.
export function clearEntries()
{
	console.log("Clearing entries...");
	document.getElementById(ENTRIES_ID).remove();
	document.getElementById("date").innerHTML = "Last Updated: DD/MM/YYYY";

	const entries = document.createElement("section");
	const pinned = document.createElement("section");

	entries.id = ENTRIES_ID;
	pinned.id = PINNED_ID;
	entries.appendChild(pinned);

	const addons = document.getElementById("addons");
	addons.insertAdjacentElement("beforeend", entries);
}

// Builds all the addon entries.
export function initEntries()
{
	if (IsBuilding)
		return;

	IsBuilding = true;
	clearEntries();

	// Show loading text
	const loading = document.getElementById("loading");
	loading.style.height = "100%";
	loading.style.visibility = "visible";
	loading.children[0].innerHTML = "Loading...";
	loading.children[1].innerHTML = "Building entries...";

	// Only show entries when done building
	const entries = document.getElementById(ENTRIES_ID);
	entries.style.visibility = "hidden";

	// Last updated
	getDate()
	buildEntries()

	/*
	TODO: rework hash
	if (targetEntry != null)
	{
		// Go to entry
		let tE = document.getElementById(targetEntry)
		tE.scrollIntoView
	{
			behaviour: "auto",
			block: "center",
			inline: "center"
		});
		tE.classList.add("highlight");
	}
	*/

	//createTagButtons();
}

async function getDate()
{
	const date = document.getElementById("date");
	const last_updated = await fetch(API_LINK + "/get-date");
	if (last_updated.status == 429)
	{
		alert('API backend returned code 429 (Too many requests). Try again in a bit. (preferably 1 minute)');
		return;
	}
	date.innerHTML = "Last Updated: " + await last_updated.text();
}

async function buildEntries()
{
	const search = new URLSearchParams(SearchArgs);
	const response = await fetch(
		API_LINK
			+ "/get-" + Settings["max-entries"] + "-" + Settings["max-entries"] * PageIndex
			+ "?" + search
	);
	if (response.status == 429)
	{
		alert('API backend returned code 429 (Too many requests). Try again in a bit. (preferably 1 minute)');
		return;
	}
	const entry_list = await response.json();
	console.log(entry_list.list);

	if (MaxPages != entry_list.pages)
		updatePageSelect(PageIndex, entry_list.pages);

	for (const entry in entry_list.list)
	{
		const e = entry_list.list[entry];
		addEntry(e);
	}

	IsBuilding = false;

	const loading = document.getElementById("loading");
	if (entry_list.list.length == 0)
	{
		loading.children[0].innerHTML = "No valid entries found...";
		loading.children[1].innerHTML = "Try changing your filters.";
	}
	else
	{
		// Hide loading text and show entries
		loading.style.height = 0;
		loading.style.visibility = "hidden";
	}

	// if the results aren't correct, try again
	if (String(search) != String(new URLSearchParams(SearchArgs)))
		initEntries();

	const entries = document.getElementById(ENTRIES_ID);
	entries.style.visibility = "visible";
}

function addEntry(e)
{
	/*
	Entry variables:
	title
	credits
	dependencies
	flairs
	description
	flag
	tags
	*/

	console.log("Adding entry: " + e.title);
	let section_id = ENTRIES_ID;

	// create a new addon
	const entry = document.createElement("section");
	entry.classList.add("entry");

	// Check for flags
	if (e.flag)
	{
		entry.classList.add(e.flag);

		if (e.flag == PINNED_ID)
			section_id = PINNED_ID;
	}

	// Set id
	const id = e.title.replace(/\W/g, "").toLowerCase() + "-" + e.credits.replace(/^by /g, "").replace(/\W/g, "").toLowerCase();
	entry.id = id;

	// Create the essentials
	const header = document.createElement("div"); // Header
	const headerExpand = document.createElement("img"); // Header Expand Icon
	const headerText = document.createElement("div"); // Header Text
	const title = document.createElement("h3"); // Title
	const credits = document.createElement("h5"); // Credits
	const fancyFlairs = createFancyFlairs(e);
	const flairs = createFlairs(e.flairs); //Flairs
	const desc = document.createElement("div"); // Description
	const tags = document.createElement("p"); // Tags

	// Set values
	header.classList.add("entry-header");
	headerExpand.classList.add("entry-expand");
	headerExpand.src = "icons/chevron-down.svg";
	title.innerHTML = e.title;
	credits.innerHTML = e.credits;
	desc.appendChild(document.createElement("hr"));
	desc.innerHTML += e.description;
	desc.classList.add("description");

	// Hide description
	desc.style.display = "none";

	// Expand description on press
	headerExpand.addEventListener(
		"click",
		() =>
		{
			if (desc.style.display === "none")
			{
				desc.style.display = "inherit";
				headerExpand.src = "icons/chevron-up.svg";
			}
			else
			{
				desc.style.display = "none";
				headerExpand.src = "icons/chevron-down.svg";
			}
		}
	);

	// Add tags
	if (e.tags)
	{
		let txt = "tags: ";
		for (let i = 0; i < e.tags.length; i++)
		{
			txt = txt + e.tags[i];
			if (i + 1 != e.tags.length)
				txt = txt + ", ";
		}
		tags.innerHTML = txt;
		tags.classList.add("entry-tags");
	}

	// Add stuff for devs
	const entryLink = createIconButton("icons/link.svg", "Copy link to entry");
	const entryId = createIconButton("icons/hash.svg", "Copy entry id");

	// Append stuff to entry
	console.log("Appending elements to addon entry...");
	if (fancyFlairs !== null)
		title.appendChild(fancyFlairs);

	title.appendChild(entryLink);
	title.appendChild(entryId);
	headerText.appendChild(title);
	headerText.appendChild(credits);
	header.appendChild(headerExpand);
	header.appendChild(headerText);
	entry.appendChild(header);

	if (flairs !== null)
		entry.appendChild(flairs);

	entry.appendChild(tags);
	entry.appendChild(desc);


	entryLink.addEventListener(
		"click",
		() =>
		{
			// just in case
			const link = window.location.href.split("#")[0] + "#" + id;
			navigator.clipboard.writeText(link);
			entryLink.children[1].innerHTML = "Copied!"
		}
	);
	entryId.addEventListener(
		"click",
		() =>
		{
			navigator.clipboard.writeText("#" + id);
			entryId.children[1].innerHTML = "Copied!"
		}
	);

	// Add the entry
	const target = document.getElementById(section_id);
	target.insertAdjacentElement("beforeend", entry);
}

function createFancyFlairs(e)
{
	const flairs = document.createElement("div");
	flairs.classList.add("infobox");

	if (e.dependencies !== undefined)
	{
		const dep = createIconButton("icons/alert-octagon.svg", "Show dependencies");
		dep.classList.add("important");
		dep.addEventListener(
			"click",
			() =>
			{
				const dialog = document.getElementById("popup");
				let content = "<h3>Dependencies</h3><hr><ul>";
				for (const i in e.dependencies)
				{
					console.log("dep:" + i);
					content += "<li>" + e.dependencies[i] + "</li>";
				}
				content += "</ul>";
				dialog.innerHTML = content;
				showDialog(null);
			}
		);
		flairs.appendChild(dep);
	}

	// wow this is broken as shit
	if (e.flag === "bad")
	{
		const bad = createIconButton("icons/x-octagon.svg", "Broken");
		bad.classList.add("bad");
		flairs.appendChild(bad);
	}

	return flairs;
}

function createIconButton(icon, string)
{
	const a = document.createElement("a");
	a.classList.add("icon-button");

	const img = document.createElement("img");
	img.src = icon;

	const span = document.createElement("span");
	span.classList.add("tooltip");
	a.onmouseover = event =>
		span.innerHTML = string;

	a.appendChild(img);
	a.appendChild(span);
	return a;
}

function createFlairs(flairs)
{
	console.log("Creating flairs...");
	if (flairs === undefined)
		return null;

	const fs = document.createElement("flairs");
	for (const f in flairs)
	{
		const div = document.createElement("div");
		console.log("Appending flair \"" + flairs[f] + "\" with text: \"" + f + "\"...");

		div.innerHTML = f;
		div.classList.add(flairs[f]);

		fs.appendChild(div);
	}

	return fs;
}

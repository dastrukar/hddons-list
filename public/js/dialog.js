var DialogCallback = () => {};

export function initDialog()
{
	const dialog = document.getElementById("popup");
	dialog.addEventListener(
		"click",
		() =>
		{
			const rect = dialog.getBoundingClientRect();
			const isInDialog = (
				rect.top <= event.clientY
				&& event.clientY <= rect.top + rect.height
				&& rect.left <= event.clientX
				&& event.clientX <= rect.left + rect.width
			);

			if (isInDialog || event.clientY == 0 || event.clientX == 0)
				return;

			if (DialogCallback)
				DialogCallback();

			dialog.close();
		}
	);
}

export function showDialog(callback)
{
	const dialog = document.getElementById("popup");
	DialogCallback = callback;
	dialog.showModal();
}

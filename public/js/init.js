import { initDialog } from "./dialog.js";
import { createTagButtons } from "./tags.js";
import { initHash, processHash } from "./hash.js";
import { initSearch } from "./search.js";
import { initSettingsButton, updateSettings } from "./settings.js";
import { initEntries } from "./entries.js";
import { initPageButtons } from "./page.js";
import { initSidebarButton } from "./sidebar.js";

// Initialising stuff

document.addEventListener(
	"DOMContentLoaded",
	(event) =>
	{
		initDialog();
		initSearch();
		initHash();
		initPageButtons();
		initSettingsButton();
		initSidebarButton();

		createTagButtons();
		processHash();
		updateSettings();
		initEntries();
	}
);

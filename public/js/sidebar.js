// Collapsable sidebar

export function initSidebarButton()
{
	const sButton = document.getElementById("sidebarbutton");
	sButton.onclick = () =>
	{
		const sidebar = document.getElementById("sidebar");

		if (sidebar.style.visibility == "visible")
		{
			sButton.style.left = 0;
			sButton.classList.remove("open");
			sidebar.style.visibility = "hidden";
		}
		else
		{
			sButton.style.left = "270px";
			sButton.classList.add("open");
			sidebar.style.visibility = "visible";
		}
	}
}

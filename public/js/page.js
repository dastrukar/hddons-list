import { IsBuilding, initEntries } from "./entries.js";

// Handles what entries should be shown

export var PageIndex = 0;
export var MaxPages = 0;

export function initPageButtons()
{
	const pages = document.getElementById("pages");
	pages.addEventListener(
		"change",
		(event) =>
		{
			console.log("page:" + event.target.value);
			PageIndex = Number(event.target.value);
			initEntries();
		}
	);

	const prevbutton = document.getElementById("prevpage");
	const nextbutton = document.getElementById("nextpage");
	prevbutton.onclick = () =>
	{
		clampPageIndex();
		if (IsBuilding || PageIndex - 1 < 0)
			return;

		--PageIndex;
		updatePageNumber();
		initEntries();
	};

	nextbutton.onclick = () =>
	{
		clampPageIndex();
		if (IsBuilding || PageIndex + 1 >= MaxPages)
			return;

		++PageIndex;
		updatePageNumber();
		initEntries();
	};
}

function clampPageIndex()
{
	if (PageIndex >= MaxPages)
		PageIndex = MaxPages - 1;

	if (PageIndex < 0)
		PageIndex = 0;
}

function updatePageNumber()
{
	const pages = document.getElementById("pages");
	pages.value = PageIndex;
}

// This should only be called when `MaxPages` changes value
export function updatePageSelect(page, max)
{
	PageIndex = page;
	MaxPages = max;
	clampPageIndex();

	const pages = document.getElementById("pages");
	pages.textContent = "";

	for (let i = 0; i < MaxPages; i++)
	{
		const option = document.createElement("option");
		option.textContent = String(i + 1);
		option.value = i;
		pages.appendChild(option);

		if (i == PageIndex)
			pages.value = option.value;
	}

	pages.value = PageIndex;
}

import { clearHash } from "./hash.js";
import { initEntries } from "./entries.js";
import { MaxPages, updatePageSelect } from "./page.js";

// Handles the search bar logic

export var SearchArgs = {
	"search": "",
	"credits": "",
	"tags": [],
	"ids": []
};

export function initSearch()
{
	const bar = document.getElementById("searchBar");
	const button = document.getElementById("searchButton");

	button.addEventListener(
		"click",
		() => SearchEntries(bar.value)
	);

	bar.addEventListener(
		"keydown",
		() =>
		{
			if (event.key != "Enter")
				return;

			SearchEntries(bar.value);
			bar.blur();
		}
	);
}

export function clearSearch()
{
	const bar = document.getElementById("searchBar");
	bar.value = "";

	SearchArgs.search = "";
	SearchArgs.credits = "";
	SearchArgs.tags = [];
	SearchArgs.ids = [];
}

export function appendSearchTag(tag)
{
	SearchArgs.tags.push(tag);
}

export function removeSearchTag(tag)
{
	SearchArgs.tags.splice(SearchArgs.tags.indexOf(tag), 1);
}

export function SearchEntries(str)
{
	console.log("GOT: " + str);
	clearHash();
	SearchArgs.search = str;
	updatePageSelect(0, MaxPages);
	initEntries();
}

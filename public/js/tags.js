import { API_LINK } from "./global_vars.js";
import { SearchArgs, appendSearchTag, removeSearchTag, clearSearch } from "./search.js";
import { PageIndex, MaxPages, updatePageSelect } from "./page.js";
import { clearHash } from "./hash.js";
import { initEntries } from "./entries.js";

// Shitty tag system that I thought of
// Maybe when I get to working on a server of sorts, I'll properly implement tags :]

// So that was a fucking lie
// EDIT: it wasn't a lie

export async function createTagButtons()
{
	const div = document.getElementById("tags");
	const response = await fetch(API_LINK + "/get-tags");
	if (response.status == 429)
	{
		alert('API backend returned code 429 (Too many requests). Try again in a bit. (preferably 1 minute)');
		return;
	}
	const tags = await response.json();
	console.log(tags);

	const text = document.createElement("p");
	const clear = document.createElement("a");
	text.innerHTML = "Filter by tags:";
	clear.innerHTML = "(clear filters)";

	text.appendChild(clear);
	div.appendChild(text);
	div.classList.add("tags");
	for (let i = 0; i < tags.length; i++) {
		const label = document.createElement("label");
		label.innerHTML = tags[i];

		const input = document.createElement("input");
		input.type = 'checkbox';
		input.name = tags[i];

		input.addEventListener(
			"change",
			() =>
			{
				if (input.checked)
					appendSearchTag(input.name);

				else
					removeSearchTag(input.name);

				console.log(SearchArgs.tags);
				updatePageSelect(0, MaxPages);
				clearHash();
				initEntries();
			}
		);

		const checkmark = document.createElement("span");
		checkmark.classList.add("checkmark");

		label.appendChild(input);
		label.appendChild(checkmark);
		div.appendChild(label);
	}

	clear.addEventListener(
		"click",
		() =>
		{
			for (let i = 1; i < div.children.length; i++)
			{
				div.children[i].children[0].checked = false;
			}

			// remove any extra stuff on the link
			clearHash();
			clearSearch();

			initEntries();
		}
	);
}

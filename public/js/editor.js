import { updateSettings } from "./settings.js";

const API_LINK = "https://hddons-list-api.vercel.app"
async function createTagButtons(selected)
{
	const div = document.getElementById("tags");
	const response = await fetch(API_LINK + "/get-tags");
	if (response.status == 429)
	{
		alert('API backend returned code 429 (Too many requests). Try again in a bit. (preferably 1 minute)');
		return;
	}
	const tags = await response.json();
	// const tags = [
	// 	"tag 1",
	// 	"tag 2",
	// 	"tag 23",
	// 	"tag 233",
	// 	"tag 2333",
	// 	"tag 23333",
	// 	"tag 233333",
	// 	"tag 2333333",
	// 	"tag 23333333",
	// ];

	for (const tag of tags)
	{
		if (tag === "")
			continue;

		const label = document.createElement("label");
		label.innerHTML = tag;

		const input = document.createElement("input");
		input.type = "checkbox";
		input.value = tag;
		input.checked = (selected != undefined && selected.includes(tag));

		const checkmark = document.createElement("span");
		checkmark.classList.add("checkmark");

		label.appendChild(input);
		label.appendChild(checkmark);
		div.appendChild(label);
	}
}

function createDeleteButton()
{
	const deleteButton = document.createElement("button");
	const deleteIcon = document.createElement("img");
	deleteButton.classList.add("deletebutton");
	deleteIcon.src = "icons/trash-2.svg";
	deleteButton.appendChild(deleteIcon);

	return deleteButton;
}

function addTag()
{
	const input = document.createElement("input");
	const deleteButton = createDeleteButton();
	input.placeholder = "insert tag";

	const section = document.createElement("section");
	section.classList.add("flexdiv");
	section.appendChild(input);
	section.appendChild(deleteButton);

	deleteButton.onclick = () => section.remove();

	const div = document.getElementById("tags");
	div.appendChild(section);
}

function addDependency(name, link)
{
	const nameInput = document.createElement("input");
	const linkInput = document.createElement("input");
	const deleteButton = createDeleteButton();
	nameInput.placeholder = "insert dependency name";
	linkInput.placeholder = "insert link/entry id";
	nameInput.classList.add("nameinput");
	linkInput.classList.add("linkinput");
	if (name != undefined)
		nameInput.value = name;

	if (link != undefined)
		linkInput.value = link;

	const topSection = document.createElement("section");
	const dependency = document.createElement("section");
	topSection.appendChild(nameInput);
	topSection.appendChild(deleteButton);
	dependency.appendChild(topSection);
	dependency.appendChild(linkInput);
	topSection.classList.add("flexdiv");
	dependency.classList.add("dependency");

	deleteButton.onclick = () => dependency.remove();

	const dependencies = document.getElementById("dependencies");
	dependencies.appendChild(dependency);
}

function addFlair(type, name)
{
	const typeInput = document.createElement("select");
	const defaultOption = document.createElement("option");
	defaultOption.value = "";
	defaultOption.text = "<type>";
	typeInput.appendChild(defaultOption);
	["nice", "bad", "important"].forEach((e) => {
		const option = document.createElement("option");
		option.value = e;
		option.text = e;
		typeInput.appendChild(option);
	});
	if (type != undefined)
		typeInput.value = type;

	const textInput = document.createElement("input");
	const deleteButton = createDeleteButton();
	textInput.placeholder = "insert flair text";
	if (name != undefined)
		textInput.value = name;

	const section = document.createElement("section");
	section.appendChild(typeInput);
	section.appendChild(textInput);
	section.appendChild(deleteButton);
	section.classList.add("flexdiv");
	section.classList.add("flair");

	deleteButton.onclick = () => section.remove();

	const flairs = document.getElementById("flairs");
	flairs.appendChild(section);
}

function getDependencyInfo(e)
{
	let info = {};
	info.name = e.querySelector(".nameinput").value;
	info.link = e.querySelector(".linkinput").value;
	return info;
}

// must have the following filled out:
// title, credits, tags, description
function validateForm()
{
	let isValid = true;
	const errors = document.getElementById("errors");
	const title = document.getElementById("title");
	const credits = document.getElementById("credits");
	const tags = document.getElementById("tags");
	const dependencies = document.getElementById("dependencies");
	const flairs = document.getElementById("flairs");
	const description = document.getElementById("description");

	errors.textContent = "";

	const removeEmpty = (e) => e.target.classList.remove("inputempty");

	if (title.value == "")
	{
		isValid = false;
		title.classList.add("inputempty");
		title.addEventListener("focus", removeEmpty);
	}

	if (credits.value == "")
	{
		isValid = false;
		credits.classList.add("inputempty");
		credits.addEventListener("focus", removeEmpty);
	}

	let tagsSelected = 0;
	for (const tag of tags.children)
	{
		const tagInput = tag.querySelector("input");
		tagsSelected += ((tag.nodeName == "LABEL" && tagInput.checked) || (tag.nodeName != "LABEL"));
		if (tag.nodeName == "LABEL" || tagInput.value != "")
			continue;

		isValid = false;
		tagInput.classList.add("inputempty");
		tagInput.addEventListener("focus", removeEmpty);
	}

	if (tagsSelected == 0)
	{
		isValid = false;
		const removeEmptyFromTag = () => tags.classList.remove("inputempty");
		tags.classList.add("inputempty");
		tags.parentElement.querySelector("button").addEventListener("click", removeEmptyFromTag);
		for (const tag of tags.children)
		{
			tag.querySelector("input").addEventListener("change", removeEmptyFromTag);
		}
	}

	for (const dependency of dependencies.children)
	{
		const nameinput = dependency.querySelector(".nameinput");
		const linkinput = dependency.querySelector(".linkinput");
		if (nameinput.value == "")
		{
			isValid = false;
			nameinput.classList.add("inputempty");
			nameinput.addEventListener("focus", removeEmpty);
		}

		if (linkinput.value == "")
		{
			isValid = false;
			linkinput.classList.add("inputempty");
			linkinput.addEventListener("focus", removeEmpty);
		}
	}

	for (const flair of flairs.children)
	{
		const select = flair.querySelector("select");
		const input = flair.querySelector("input");
		if (select.value == "")
		{
			isValid = false;
			select.classList.add("inputempty");
			select.addEventListener("focus", removeEmpty);
		}

		if (input.value == "")
		{
			isValid = false;
			input.classList.add("inputempty");
			input.addEventListener("focus", removeEmpty);
		}
	}

	if (description.value == "")
	{
		isValid = false;
		description.classList.add("inputempty");
		description.addEventListener("focus", removeEmpty);
	}

	if (!isValid)
		errors.textContent = "please fill in the required forms (marked red)";

	return isValid;
}

function compileToYAML()
{
	const title = document.getElementById("title");
	const credits = document.getElementById("credits");
	const dependencies = document.getElementById("dependencies");
	const tags = document.getElementById("tags");
	const flairs = document.getElementById("flairs");
	const description = document.getElementById("description");

	const fixQuotes = (str) => str.replaceAll(/"/g, '\\"');
	let output = `title: "${fixQuotes(title.value)}"\n`;
	output += `credits: "${fixQuotes(credits.value)}"\n`;

	if (dependencies.children.length > 0)
		output += `dependencies:\n`;

	for (const dependency of dependencies.children)
	{
		const nameInput = dependency.querySelector(".nameinput");
		const linkInput = dependency.querySelector(".linkinput");
		output += `    - "[${fixQuotes(nameInput.value)}](${fixQuotes(linkInput.value)})"\n`;
	}

	output += `tags:\n`;
	for (const tag of tags.children)
	{
		const tagInput = tag.querySelector("input");
		if (tag.nodeName != "LABEL" || tagInput.checked)
			output += `    - "${fixQuotes(tagInput.value)}"\n`;
	}

	if (flairs.children.length > 0)
		output += `flairs:\n`;

	for (const flair of flairs.children)
	{
		const type = flair.querySelector("select");
		const text = flair.querySelector("input");
		output += `    "${fixQuotes(text.value)}": "${type.value}"\n`;
	}

	output = output + `description: |\n`;
	const getNextIndex = () => description.value.indexOf("\n", prevIndex);
	const processDescription = (str) => (str == "\n")? `${str}` : `    ${str}`;
	let prevIndex = 0;
	let nextIndex = getNextIndex();
	while (nextIndex != -1)
	{
		output += processDescription(description.value.slice(prevIndex, nextIndex + 1));
		prevIndex = nextIndex + 1;
		if (prevIndex > description.value.length)
			break;

		nextIndex = getNextIndex();
	}

	if (prevIndex < description.value.length)
		output += processDescription(description.value.slice(prevIndex));

	return output;
}

const regexReplace = /[!@#$%^&*()/\\'"\[\]{}:;.,]/g;
function generateFilename()
{
	const filename = document.getElementById("filename");
	if (filename.value != "")
		return filename.value;

	const title = document.getElementById("title");
	const credits = document.getElementById("credits");
	return `${title.value.replaceAll(" ", "_")} - ${credits.value.replace("by ", "").replaceAll(" ", "_")}`.replaceAll(regexReplace, "") + `.yaml`;
}

function clearForm()
{
	const title = document.getElementById("title");
	const credits = document.getElementById("credits");
	const filename = document.getElementById("filename");
	const dependencies = document.getElementById("dependencies");
	const tags = document.getElementById("tags");
	const flairs = document.getElementById("flairs");
	const description = document.getElementById("description");
	title.textContent = "";
	title.value = "";
	credits.textContent = "";
	credits.value = "";
	filename.textContent = "";
	filename.value = "";
	dependencies.innerHTML = "";
	flairs.innerHTML = "";
	description.textContent = "";
	description.value = "";

	let markedDelete = [];
	for (const tag of tags.children)
	{
		if (tag.nodeName == "LABEL")
		{
			const tagInput = tag.querySelector("input");
			tagInput.checked = false;
			continue;
		}

		markedDelete.push(tag);
	}

	markedDelete.forEach((e) => e.remove());
	updateFilenamePlaceholder();
}

function exportAsYAML()
{
	if (!validateForm())
		return;

	const yaml = compileToYAML();
	const filename = generateFilename();
	const file = new File([yaml], filename, { type: "text/plain" });
	const exportURL = URL.createObjectURL(file);
	console.log(filename);

	// hacky way to force the user to download a file/blob
	const a = document.createElement("a");
	a.href = exportURL;
	a.download = filename;
	a.click();
	URL.revokeObjectURL(exportURL);
}

function updateFilenamePlaceholder()
{
	let input = document.getElementById("filename");
	if (input.value == "")
		input.placeholder = "default will be: " + generateFilename();
}

document.addEventListener(
	"DOMContentLoaded",
	(event) =>
	{
		const title = document.getElementById("title");
		const credits = document.getElementById("credits");
		const filename = document.getElementById("filename");
		const description = document.getElementById("description");
		title.addEventListener("change", updateFilenamePlaceholder);
		credits.addEventListener("change", updateFilenamePlaceholder);
		filename.addEventListener("change", updateFilenamePlaceholder);
		updateFilenamePlaceholder();

		const tagButton = document.getElementById("add-tag");
		const dependencyButton = document.getElementById("add-dependency");
		const flairButton = document.getElementById("add-flair");
		tagButton.onclick = () => addTag();
		dependencyButton.onclick = () => addDependency(undefined, undefined);
		flairButton.onclick = () => addFlair(undefined, undefined);

		// checks if there's a id in the link
		const hash = location.hash.substr(1);
		let tags = [];
		const setEntryData = async () =>
		{
			const response = await fetch(`${API_LINK}/get-1-0?ids=${hash}`);
			if (response.status == 429)
			{
				alert('API backend returned code 429 (Too many requests). Try again in a bit. (preferably 1 minute)');
				return;
			}
			const json = await response.json();
			const entry = json.list[0];
			title.value = entry.title;
			credits.value = entry.credits;
			filename.value = entry.filename.replace("entries/", "");
			createTagButtons(entry.tags);

			entry.dependencies = (entry.dependencies == undefined)? [] : entry.dependencies;
			for (const dep of entry.dependencies)
			{
				const tmpdiv = document.createElement("div");
				tmpdiv.innerHTML = dep;
				const depElement = tmpdiv.querySelector("a");
				addDependency(depElement.textContent, depElement.href.replace(document.URL.replace(/#.+/, ""), ""));
			}

			entry.flairs = (entry.flairs == undefined)? {} : entry.flairs;
			for (const f in entry.flairs)
			{
				const tmpdiv = document.createElement("div");
				tmpdiv.innerHTML = f;
				addFlair(entry.flairs[f], tmpdiv.querySelector("p").textContent);
			}

			const rawResponse = await fetch(`https://gitlab.com/api/v4/projects/29609866/repository/files/entries%2F${filename.value}/raw?ref=master`);
			const fileText = await rawResponse.text();
			description.value = fileText.replace(/.*description: \|\n/s, "").replaceAll(/^    /gm, "");
		}

		if (hash.length > 0)
			setEntryData();

		else
			createTagButtons();

		updateSettings();
	}
);


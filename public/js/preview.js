// basically a very stripped down version of buildEntries
document.addEventListener(
	"DOMContentLoaded",
	function(event)
	{
		let file = document.getElementById("fileinput");

		file.addEventListener(
			"change",
			function()
			{
				// Get file input
				let reader = new FileReader();
				reader.onload =
					function(e)
					{
						clearEntries();
						let file = JSON.parse(e.target.result);

						// About
						let about = document.getElementById("about")
						about.innerHTML = file.about;

						// Last updated
						let date = document.getElementById("date");
						date.innerHTML = "Last Updated: " + file.last_updated;

						for (let entry in file.list)
						{
							let ent = file.list[entry];
							console.log(ent);

							addEntry(ent);
						}
					};

				reader.readAsText(this.files[0]);
			}
		);
	}
);

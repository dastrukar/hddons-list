from flask import Flask, request
from flask_cors import CORS
from markupsafe import escape
from datetime import datetime, timezone
from threading import Lock
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import re
import json
import requests
import os

# welcome to the world's worst website backend

MAX_ENTRIES_PER_REQUEST = 100
CHECKSUM_URL = r'https://gitlab.com/api/v4/projects/31095835/repository/files/entries.md5/raw?ref=master'
LIST_URL = r'https://gitlab.com/api/v4/projects/31095835/repository/files/entries_list.json/raw?ref=master'

HDDONS_LIST_REDIS_URL = os.getenv('HDDONS_LIST_REDIS_URL')
if not HDDONS_LIST_REDIS_URL:
	HDDONS_LIST_REDIS_URL = 'redis://127.0.0.1:6379'

# Setup
app = Flask(__name__)
CORS(app)
limiter = Limiter(
	get_remote_address,
	app=app,
	default_limits=['1200 per hour', '40 per minute', '3 per second'],
	storage_uri=HDDONS_LIST_REDIS_URL,
)

# global variables
ids = []
entries_cache = {}
entries_md5 = ""
last_fetch = datetime.today()
update_lock = Lock() # not sure if it's a good idea, but got to prevent me data races

def lock_update():
	global update_lock
	update_lock.acquire()

def unlock_update():
	global update_lock
	update_lock.release()

def get_id(e):
	return re.sub(r'\W', '', e["title"]).lower() + "-" + re.sub(r'\W', "", re.sub(r'^by ', '', e["credits"])).lower()

@app.route("/")
def test():
	return f"lorem ipsum. this is an api thing for hddons-list. hello. last fetch was at: {last_fetch} with checksum: {entries_md5}"

@app.route("/get-tags")
def get_tags():
	global entries_cache
	update_entries()
	tags = list({x for e in entries_cache['list'] for x in e['tags'] if len(x) > 0})
	tags.sort()
	return json.dumps(tags)

@app.route("/get-date")
def get_date():
	global entries_cache
	update_entries()
	r = entries_cache.get("last_updated")
	return r

@app.route("/get-<amt>-<offset>")
def get_entries(amt, offset):
	update_entries()
	entries = search_args(request.args)

	max = len(entries)
	amt = int(escape(amt))
	offset = int(escape(offset))
	range_end = offset + amt

	if amt <= 0:
		return { "pages": 0, "list": [] }

	elif amt > MAX_ENTRIES_PER_REQUEST:
		return { "pages": 0, "list": [] }

	if range_end >= max:
		range_end = max

	if offset >= max:
		offset = max

	ret = {
		"pages": get_pages(amt, max),
		"list": [],
	}

	for i in range(offset, range_end):
		ret["list"].append(entries[i])

	return ret

# division causes rounding issues, so we'll just manually count it
def get_pages(amt, max):
	pages = 0
	while max > 0:
		max -= amt
		pages += 1

	return pages

def search_args(args):
	global entries_cache

	search = args.get('search')
	tags = args.get('tags')
	credits = args.get('credits')
	ids = args.get('ids')

	print("search:" + escape(search))
	print("tags:" + escape(tags))
	print("credits:" + escape(credits))
	print("ids:" + escape(ids))

	# return the default list if there's no args
	if (
		not search
		and not tags
		and not credits
		and not ids
	):
		return entries_cache["list"]

	if search:
		search = search.lower()
	if tags:
		tags = tags.lower().split(',')
	if credits:
		credits = credits.lower()

	entry_list = []
	for e in entries_cache["list"]:

		if (search and e["title"].lower().find(search) != -1):
			print(f"found {search} in {e['title']}")
		elif search:
			continue

		if tags:
			valid_count = len(tags)
			count = 0
			for t in e['tags']:
				for st in tags:
					if t.lower() != st:
						continue

					count += 1
					print(f"found {tags} in {e['title']}")
					break

				if count == valid_count:
					break

			if count != valid_count:
				continue

		if (credits and e["credits"].lower().find(credits) != -1):
			print(f"found {credits} in {e['title']}")
		elif credits:
			continue

		if (ids and not get_id(e) in ids):
			continue

		entry_list.append(e)

	return entry_list

@app.route("/update")
def update_entries():
	global entries_cache
	global entries_md5
	global last_fetch
	global ids
	lock_update()

	print("attempting to fetch entries")
	cr = requests.get(CHECKSUM_URL)
	if (entries_md5 == cr.text):
		print("no checksum difference, stopping fetch")
		unlock_update()
		return

	entries_md5 = cr.text
	er = requests.get(LIST_URL)
	entries_cache = json.loads(er.text)
	ids = []
	for e in entries_cache["list"]:
		ids.append(get_id(e))
	last_fetch = datetime.now(timezone.utc).strftime("%Y-%m-%d | %H:%M:%S.%f %Z")
	print("fetched entries at " + last_fetch)

	unlock_update()
	return str(cr.status_code)

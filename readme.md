## What?
This is an experimental addons list fetcher thing.

The idea is to use a seperate repository to store the addons list. Ideally, each addon has its own "entry".
The entry will contain details about the mod itself.
All entries will then be processed by GitLab CI into a list of entries.
This list of entries will then be read by the website, becoming an addon entry on the website.

The whole point of this is to, hopefully, allow other people to easily update their own addon details via PRs/MRs, without having to interfere with the website stuff.
Also, you don't have to manually sort mods alphabetically, because the code does it all :p

The limitations from this current system are (at least from what i can tell): 
-  You can't sort addons manually. (some oddities might occur and you can't do anything about it :])
- It's a bit more slower than just pure HTML. (even more slower if your internet sucks ass)
- You need at least know how to write some YAML syntax. (well, it ain't that hard tbh)

You win some, and you lose some, I guess.

[Here's the repository that's being used.](https://gitlab.com/dastrukar/hddons-list-entries)


*yes, this is copied from the about entry.*

Credits:
- Colour scheme used: [Nord](https://www.nordtheme.com)
- Git Logo by Jason Long (taken from [git-scm.com](https://git-scm.com/downloads/logos))
- [Feather Icons](https://feathericons.com/) for most of the icons
- HDest and ZDoom svg logo by dastrukar
